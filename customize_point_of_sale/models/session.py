from odoo import models, fields ,api

class pointOfSaleSession(models.Model):
    _inherit = 'pos.session'

    total_amount = fields.Float('Amount' , compute = "get_total_entry")


    @api.multi
    def get_total_entry(self):
        for rec in self:
            for line in rec.statement_ids:
                rec.total_amount += line.total_entry_encoding




